/*
Navicat MySQL Data Transfer

Source Server         : nodeProject
Source Server Version : 50736
Source Host           : localhost:3306
Source Database       : help_db

Target Server Type    : MYSQL
Target Server Version : 50736
File Encoding         : 65001

Date: 2023-05-13 18:27:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lostpropertyorder
-- ----------------------------
DROP TABLE IF EXISTS `lostpropertyorder`;
CREATE TABLE `lostpropertyorder` (
  `lostOrFindOrderId` varchar(255) NOT NULL COMMENT '失物单号',
  `uid` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL COMMENT '用户id',
  `lostOrFindAddress` varchar(255) DEFAULT NULL COMMENT '丢失地址',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `targetAddress` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `describe` varchar(255) DEFAULT NULL COMMENT '物品描述',
  `lostOrFindTime` datetime DEFAULT NULL COMMENT '丢失时间',
  `lostOrderType` varchar(255) DEFAULT NULL COMMENT '失物招领类型',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`lostOrFindOrderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lostpropertyorder
-- ----------------------------

-- ----------------------------
-- Table structure for lost_property_order
-- ----------------------------
DROP TABLE IF EXISTS `lost_property_order`;
CREATE TABLE `lost_property_order` (
  `lostOrFindOrderId` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '失物单号',
  `uid` char(36) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户id',
  `lostOrFindAddress` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '丢失地址或拾到地址',
  `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '联系电话',
  `targetAddress` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '联系地址',
  `remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `describe` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '物品描述',
  `lostOrFindTime` datetime DEFAULT NULL COMMENT '丢失时间或拾到时间',
  `lostOrderType` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '失物招领类型，0代表失物，1代表寻物',
  `createdAt` datetime NOT NULL,
  `propertyPicture` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`lostOrFindOrderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lost_property_order
-- ----------------------------
INSERT INTO `lost_property_order` VALUES ('FIND20230513013635', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '空中', '19330668663', '13舍', '白色的战斗机', '丢了一架飞机', '2023-05-08 08:35:00', '1', '2023-05-13 01:36:35', 'http://192.168.191.1:3001/api/public/getFiles?id=08dc1837943f0d3a91c4e0ab14e731bb.jpg&&mimetype=image/jpeg', '2023-05-13 01:36:35');
INSERT INTO `lost_property_order` VALUES ('LOST20230513013242', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '三食堂门口', '19330668663', '四食堂', '艺术学院的', '毕业生档案', '2023-05-11 01:30:00', '0', '2023-05-13 01:32:42', 'http://192.168.191.1:3001/api/public/getFiles?id=d15591398608d9fba0657af23be3f64f.jpg&&mimetype=image/jpeg', '2023-05-13 01:32:42');
INSERT INTO `lost_property_order` VALUES ('LOST20230513013431', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '13舍门口', '19330668663', '13舍 666', '有三瓶，是新开的', '果酒', '2023-05-04 11:32:00', '0', '2023-05-13 01:34:31', 'http://192.168.191.1:3001/api/public/getFiles?id=fe51def193fe1001d225cbb84e84f54f.jpg&&mimetype=image/jpeg', '2023-05-13 01:34:31');
INSERT INTO `lost_property_order` VALUES ('LOST20230513014021', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '五食堂门口', '19330668663', '五食堂门口', '有三把钥匙，一个卡通人物挂件', '钥匙', '2023-05-11 10:36:00', '0', '2023-05-13 01:40:21', 'http://192.168.191.1:3001/api/public/getFiles?id=506794b49dd7746e6fba8b5ac7a227ce.jpg&&mimetype=image/jpeg', '2023-05-13 01:40:21');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `orderid` varchar(45) NOT NULL COMMENT '订单号',
  `userName` varchar(255) DEFAULT NULL COMMENT '用户姓名',
  `uid` varchar(45) NOT NULL COMMENT '用户id',
  `did` varchar(45) DEFAULT NULL COMMENT '配送员id',
  `orderType` int(11) NOT NULL DEFAULT '0' COMMENT '订单类型：0：帮我取，1:帮我寄，2:帮我买，3:万能帮',
  `sourceAddress` varchar(45) DEFAULT NULL COMMENT '源地址',
  `sourcePhone` varchar(45) DEFAULT NULL COMMENT '源电话',
  `targetAddress` varchar(45) NOT NULL COMMENT '目标地址',
  `targetPhone` varchar(45) DEFAULT NULL COMMENT '目标电话',
  `remark` varchar(100) DEFAULT NULL COMMENT '购买要求／备注信息',
  `requireTime` datetime NOT NULL COMMENT '送货时间／发货时间／取货时间／排队时间',
  `timeLong` varchar(20) DEFAULT '' COMMENT '尽快送达／所需时常（秒或毫秒）',
  `amount` double NOT NULL DEFAULT '0' COMMENT '跑腿费',
  `status` int(11) DEFAULT NULL COMMENT '订单状态：0:待接单，1:已接单，2:已完成，3:已取消',
  `createdAt` datetime NOT NULL COMMENT '创建时间',
  `updatedAt` datetime NOT NULL COMMENT '修改时间',
  `recivedAt` datetime DEFAULT NULL COMMENT '接单时间',
  `finishedAt` datetime DEFAULT NULL COMMENT '订单完成时间',
  `canceledAt` datetime DEFAULT NULL,
  `reciveCode` varchar(10) DEFAULT NULL COMMENT '收货码',
  `itemSize` varchar(45) DEFAULT NULL COMMENT '物品重量/大小',
  `itemCount` int(11) DEFAULT NULL COMMENT '快递数量',
  `sexLimit` int(11) DEFAULT '0' COMMENT '性别限制：0:不限性别 1:限男生 2:限女生',
  PRIMARY KEY (`orderid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('WL20230512143747', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', null, '0', '邮政，四食堂旁', '19330668663', '13舍', '19330668663', '帮我取一个小快递', '2023-05-14 14:37:00', '', '5', '3', '2023-05-12 14:37:47', '2023-05-13 01:13:46', null, null, '2023-05-13 01:13:46', '9773', '小于5斤', '1', '0');
INSERT INTO `order` VALUES ('WL20230512143900', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', 'f78893fd-8e2e-4741-9817-4906590e7081', '1', '12舍', '19330668663', '北京', '19330668663', '帮我寄一份文件', '2023-05-11 13:37:00', '', '5', '2', '2023-05-12 14:39:00', '2023-05-12 16:49:04', '2023-05-12 15:01:12', '2023-05-12 16:49:04', null, '1384', '小于5斤', '1', '0');
INSERT INTO `order` VALUES ('WL20230512144003', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', null, '2', '楼下水果店', '19330668663', '第三教学楼', '19330668663', '帮我买一个棒棒糖', '2023-05-10 14:39:00', '', '5', '3', '2023-05-12 14:40:03', '2023-05-13 01:13:43', null, null, '2023-05-13 01:13:43', '8267', '', '1', '0');
INSERT INTO `order` VALUES ('WL20230512144038', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', 'f78893fd-8e2e-4741-9817-4906590e7081', '3', '', '19330668663', '逸夫楼503', '19330668663', '帮我打印论文报告', '2023-05-12 14:40:00', '', '5', '1', '2023-05-12 14:40:38', '2023-05-12 14:58:16', '2023-05-12 14:58:16', null, null, '2797', '', '1', '0');
INSERT INTO `order` VALUES ('WL20230513011535', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', null, '0', '菜鸟之家旁的栏杆处', '19330668663', '13舍615', '19330668663', '麻烦帮我取一下外卖', '2023-05-07 12:13:00', '', '5', '0', '2023-05-13 01:15:35', '2023-05-13 01:15:35', null, null, null, '4749', '小于5斤', '1', '2');
INSERT INTO `order` VALUES ('WL20230513011704', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '1', '文理学院老校区教务处一楼105', '19330668663', '深圳市宝安区莲花社区', '19330668663', '辛苦帮我寄下学生档案', '2023-06-13 00:00:00', '', '5', '1', '2023-05-13 01:17:04', '2023-05-13 12:00:09', '2023-05-13 12:00:09', null, null, '5352', '小于5斤', '1', '0');
INSERT INTO `order` VALUES ('WL20230513011846', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '2', '后街花花店铺', '19330668663', '图书馆拍毕业照处', '19330668663', '帮我买束花', '2023-05-14 15:00:00', '', '5', '2', '2023-05-13 01:18:46', '2023-05-13 12:30:06', '2023-05-13 12:29:46', '2023-05-13 12:30:06', null, '8081', '', '1', '0');
INSERT INTO `order` VALUES ('WL20230513011918', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', null, '3', '', '19330668663', '逸夫楼501', '19330668663', '帮我打印报告', '2023-05-12 00:00:00', '', '5', '0', '2023-05-13 01:19:18', '2023-05-13 01:19:18', null, null, null, '7253', '', '1', '2');
INSERT INTO `order` VALUES ('WL20230513012002', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '4eb975c6-2066-4f20-804a-6f4c8c6c2d38', '0', '邮政', '19330668663', '12舍 556', '19330668663', '帮我取快递', '2023-05-10 11:00:00', '', '10', '1', '2023-05-13 01:20:02', '2023-05-13 11:30:39', '2023-05-13 11:30:39', null, null, '2882', '5-10斤', '1', '1');
INSERT INTO `order` VALUES ('WL20230513012130', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '8b35289e-b7de-4ca2-8ba1-0709e3ded847', '1', '13舍666', '19330668663', '湘潭大学', '19330668663', '帮我寄实习材料', '2023-05-13 01:20:00', '', '5', '1', '2023-05-13 01:21:30', '2023-05-13 11:13:54', '2023-05-13 11:13:54', null, null, '2643', '小于5斤', '1', '0');
INSERT INTO `order` VALUES ('WL20230513012800', '小王', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', null, '0', '空', '19330668663', '图文', '19330668663', '老两口', '2023-05-13 01:26:00', '', '5', '3', '2023-05-13 01:28:00', '2023-05-13 01:28:35', null, null, '2023-05-13 01:28:35', '1257', '小于5斤', '1', '1');
INSERT INTO `order` VALUES ('WL20230513015034', '赵盼儿', '01ed882b-efdc-4706-8c60-844d1b7b0e56', null, '2', '楼下超市', '15754317792', '12舍', '15754317792', '帮我买一包薯片', '2023-04-13 01:50:00', '', '5', '0', '2023-05-13 01:50:34', '2023-05-13 01:50:34', null, null, null, '5647', '', '1', '2');
INSERT INTO `order` VALUES ('WL20230513015414', '赵盼儿', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '0', '四食堂', '15754317792', '15舍', '15754317792', '帮我取快递', '2023-02-13 01:53:00', '', '5', '2', '2023-05-13 01:54:14', '2023-05-13 02:27:18', '2023-05-13 02:25:19', '2023-05-13 02:27:18', null, '4924', '小于5斤', '1', '0');
INSERT INTO `order` VALUES ('WL20230513112902', '陈赫', '4eb975c6-2066-4f20-804a-6f4c8c6c2d38', 'd8f59a25-3577-489a-9ef7-1cb4cad5887b', '0', '中通快递', '13538420906', '男生宿舍501', '13538420906', '【中通快递】您好，中通快递包裹已放在一楼快递处，收到短信请尽快取走，谢谢。有问题请联系18529553914', '2023-05-13 13:28:00', '', '5', '1', '2023-05-13 11:29:02', '2023-05-13 12:54:45', '2023-05-13 12:54:45', null, null, '7065', '小于5斤', '1', '1');
INSERT INTO `order` VALUES ('WL20230513125439', '王二麻子', 'd8f59a25-3577-489a-9ef7-1cb4cad5887b', '4eb975c6-2066-4f20-804a-6f4c8c6c2d38', '3', '', '17834521386', '生物学院教学楼一楼', '17834521386', '帮我打印一份资料送到生物学院教学楼一楼', '2023-05-13 13:54:00', '', '5', '1', '2023-05-13 12:54:39', '2023-05-13 14:55:00', '2023-05-13 14:55:00', null, null, '6056', '', '1', '0');
INSERT INTO `order` VALUES ('WL20230513144555', '赵盼儿', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '4eb975c6-2066-4f20-804a-6f4c8c6c2d38', '0', '回来了', '15754317792', '空', '15754317792', '秃头', '2023-05-13 14:45:00', '', '30', '1', '2023-05-13 14:45:55', '2023-05-13 14:54:51', '2023-05-13 14:54:51', null, null, '5875', '20-50斤', '1', '1');

-- ----------------------------
-- Table structure for secondgoods
-- ----------------------------
DROP TABLE IF EXISTS `secondgoods`;
CREATE TABLE `secondgoods` (
  `goodsId` varchar(255) NOT NULL COMMENT '二手商品Id',
  `uid` char(36) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL COMMENT '用户id',
  `goodsName` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `oldPrice` double DEFAULT NULL COMMENT '商品现价',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `inventory` double DEFAULT NULL COMMENT '库存',
  `degree` varchar(255) DEFAULT NULL COMMENT '新旧程度',
  `goodsDescribe` varchar(255) DEFAULT NULL COMMENT '商品描述',
  `goodsPhoto` varchar(255) DEFAULT NULL COMMENT '商品图片',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`goodsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of secondgoods
-- ----------------------------

-- ----------------------------
-- Table structure for second_goods
-- ----------------------------
DROP TABLE IF EXISTS `second_goods`;
CREATE TABLE `second_goods` (
  `goodsId` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '二手商品Id',
  `uid` char(36) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户id',
  `goodsName` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品名称',
  `newPrice` double(10,2) DEFAULT NULL,
  `oldPrice` double DEFAULT NULL COMMENT '商品现价',
  `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '联系电话',
  `inventory` double DEFAULT NULL COMMENT '库存',
  `degree` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '新旧程度',
  `goodsDescribe` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品描述',
  `goodsStatus` char(10) DEFAULT NULL COMMENT '商品状态，0未出售，1已出售',
  `goodsPhoto` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品图片',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`goodsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of second_goods
-- ----------------------------
INSERT INTO `second_goods` VALUES ('ES20230512151451', 'f78893fd-8e2e-4741-9817-4906590e7081', '置物架', '8.00', '12', '15211343695', '2', '九成新', '白色、床头置物架', '0', 'http://localhost:3001/api/public/getFiles?id=13c2f6210ed9400622e89f749d721947.avif&&mimetype=image/avif', '2023-05-12 15:14:51', '2023-05-12 15:14:51');
INSERT INTO `second_goods` VALUES ('ES20230512151555', 'f78893fd-8e2e-4741-9817-4906590e7081', '收纳神器', '8.00', '12', '15211343695', '2', '七成新', '挂在墙上的收纳架', '0', 'http://localhost:3001/api/public/getFiles?id=62f0c7f40f00ea5e66d69fcbee9ae06d.avif&&mimetype=image/avif', '2023-05-12 15:15:55', '2023-05-12 15:15:55');
INSERT INTO `second_goods` VALUES ('ES20230512151702', 'f78893fd-8e2e-4741-9817-4906590e7081', '手机充电器', '2.00', '13', '15211343695', '12', '四成新', '手机充电置物架，不易折损充电线', '0', 'http://localhost:3001/api/public/getFiles?id=7632487365599c17238adc6f0919ea80.avif&&mimetype=image/avif', '2023-05-12 15:17:02', '2023-05-12 15:17:02');
INSERT INTO `second_goods` VALUES ('ES20230512151833', 'f78893fd-8e2e-4741-9817-4906590e7081', '挂衣服神器', '2.90', '9.9', '15211343695', '2', '七成新', '可以挂在床头上的挂衣神器', '0', 'http://localhost:3001/api/public/getFiles?id=43a250e862c128a393a495033c1b562d.avif&&mimetype=image/avif', '2023-05-12 15:18:33', '2023-05-12 15:18:33');
INSERT INTO `second_goods` VALUES ('ES20230513113217', '4eb975c6-2066-4f20-804a-6f4c8c6c2d38', '蓝牙耳机', '50.00', '120', '13538420906', '1', '七成新', '白色荣耀蓝牙耳机，七成新', '0', 'http://192.168.0.103:3001/api/public/getFiles?id=fd14a8f73fe30126507e3f49954ffa81.jpg&&mimetype=image/jpeg', '2023-05-13 11:32:17', '2023-05-13 11:32:17');
INSERT INTO `second_goods` VALUES ('ES20230513124451', 'd8f59a25-3577-489a-9ef7-1cb4cad5887b', '眼镜盒', '5.00', '20', '17834521386', '1', '八成新', '绿色的眼镜盒，还很新', '0', 'http://192.168.0.103:3001/api/public/getFiles?id=6a04cb99c8dfc288f419376da0f26ac5.jpg&&mimetype=image/jpeg', '2023-05-13 12:44:51', '2023-05-13 12:44:51');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `uid` char(36) NOT NULL COMMENT '用户id',
  `userphone` varchar(45) NOT NULL COMMENT '用户账号(手机号)',
  `password` varchar(45) NOT NULL COMMENT '用户密码',
  `userRole` tinyint(1) NOT NULL DEFAULT '0' COMMENT '用户角色：0 普通用户 1 配送员 2 管理员',
  `createdAt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '是否删除 0:没删，1:已删',
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `uusername_UNIQUE` (`userphone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('01ed882b-efdc-4706-8c60-844d1b7b0e56', '15754317792', 'Vchs0bbdk2pr/Ac6DsHruw==', '0', '2023-05-13 01:46:01', '2023-05-13 01:46:01');
INSERT INTO `user` VALUES ('1883bd9b-b896-4d9c-86f6-f2b4222f033b', '19330668663', 'Vchs0bbdk2pr/Ac6DsHruw==', '0', '2023-05-12 14:24:06', '2023-05-12 14:24:06');
INSERT INTO `user` VALUES ('448a7978-d299-419c-ac54-55a699db9066', '15211642453', 'Vchs0bbdk2pr/Ac6DsHruw==', '0', '2023-05-13 02:02:03', '2023-05-13 02:02:03');
INSERT INTO `user` VALUES ('4eb975c6-2066-4f20-804a-6f4c8c6c2d38', '13538420906', 'Vchs0bbdk2pr/Ac6DsHruw==', '0', '2023-05-13 11:23:08', '2023-05-13 11:23:08');
INSERT INTO `user` VALUES ('d8f59a25-3577-489a-9ef7-1cb4cad5887b', '17834521386', 'Vchs0bbdk2pr/Ac6DsHruw==', '0', '2023-05-13 12:42:03', '2023-05-13 12:42:03');
INSERT INTO `user` VALUES ('f78893fd-8e2e-4741-9817-4906590e7081', '15211343695', 'Vchs0bbdk2pr/Ac6DsHruw==', '0', '2023-05-12 14:56:35', '2023-05-12 14:56:35');

-- ----------------------------
-- Table structure for user_address
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `id` varchar(45) NOT NULL COMMENT '地址id',
  `uid` varchar(45) NOT NULL COMMENT '用户id',
  `userAddress` varchar(100) NOT NULL COMMENT '地址',
  `createdAt` datetime NOT NULL COMMENT '创建时间',
  `updatedAt` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_address
-- ----------------------------

-- ----------------------------
-- Table structure for user_balance
-- ----------------------------
DROP TABLE IF EXISTS `user_balance`;
CREATE TABLE `user_balance` (
  `uid` varchar(45) NOT NULL COMMENT '用户id',
  `userBalance` double NOT NULL DEFAULT '0' COMMENT '用户余额',
  `updatedAt` datetime NOT NULL COMMENT '更新时间',
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_balance
-- ----------------------------
INSERT INTO `user_balance` VALUES ('01ed882b-efdc-4706-8c60-844d1b7b0e56', '220', '2023-05-13 14:49:02', '2023-05-13 01:46:01');
INSERT INTO `user_balance` VALUES ('1883bd9b-b896-4d9c-86f6-f2b4222f033b', '255', '2023-05-13 02:27:18', '2023-05-12 14:24:06');
INSERT INTO `user_balance` VALUES ('448a7978-d299-419c-ac54-55a699db9066', '0', '2023-05-13 02:02:03', '2023-05-13 02:02:03');
INSERT INTO `user_balance` VALUES ('4eb975c6-2066-4f20-804a-6f4c8c6c2d38', '95', '2023-05-13 11:29:02', '2023-05-13 11:23:08');
INSERT INTO `user_balance` VALUES ('9477a3a7-c223-457c-b9d4-2c00194defa6', '0', '2023-05-13 02:00:18', '2023-05-13 02:00:18');
INSERT INTO `user_balance` VALUES ('d8f59a25-3577-489a-9ef7-1cb4cad5887b', '15', '2023-05-13 12:54:39', '2023-05-13 12:42:03');
INSERT INTO `user_balance` VALUES ('f78893fd-8e2e-4741-9817-4906590e7081', '205', '2023-05-12 16:49:04', '2023-05-12 14:56:35');

-- ----------------------------
-- Table structure for user_balance_record
-- ----------------------------
DROP TABLE IF EXISTS `user_balance_record`;
CREATE TABLE `user_balance_record` (
  `id` varchar(45) NOT NULL COMMENT '用户交易记录id',
  `uid` varchar(45) NOT NULL COMMENT '用户id',
  `oldBalance` double NOT NULL COMMENT '老余额',
  `amount` double NOT NULL COMMENT '修改金额',
  `newBalance` double NOT NULL COMMENT '新余额',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '交易类型 0:接单入款，1:发单出款，2:充值',
  `orderId` varchar(45) NOT NULL COMMENT '交易订单流水号',
  `createdAt` datetime NOT NULL COMMENT '创建时间',
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_balance_record
-- ----------------------------
INSERT INTO `user_balance_record` VALUES ('1156abfc-0112-4906-9e7e-b23a37926dc9', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '250', '5', '245', '1', 'WL20230513012800', '2023-05-13 01:28:00', '2023-05-13 01:28:00');
INSERT INTO `user_balance_record` VALUES ('1669a640-319a-460d-a45e-3927ee018843', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '285', '5', '280', '1', 'WL20230513011704', '2023-05-13 01:17:04', '2023-05-13 01:17:04');
INSERT INTO `user_balance_record` VALUES ('1e72ad18-0ecc-4704-a2fb-a1e7c77a580f', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '275', '5', '270', '1', 'WL20230513011918', '2023-05-13 01:19:18', '2023-05-13 01:19:18');
INSERT INTO `user_balance_record` VALUES ('2d556ce4-0935-4eed-ac96-788bd54987b4', '4eb975c6-2066-4f20-804a-6f4c8c6c2d38', '100', '5', '95', '1', 'WL20230513112902', '2023-05-13 11:29:02', '2023-05-13 11:29:02');
INSERT INTO `user_balance_record` VALUES ('2e262911-2ca3-41cd-9254-b50a82abae4e', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '180', '100', '280', '2', 'CZ20230513011319', '2023-05-13 01:13:19', '2023-05-13 01:13:19');
INSERT INTO `user_balance_record` VALUES ('2e5e2f43-1f63-4f92-80e1-3146326c844f', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '280', '5', '275', '1', 'WL20230513011846', '2023-05-13 01:18:46', '2023-05-13 01:18:46');
INSERT INTO `user_balance_record` VALUES ('2f1d4b58-be43-4585-88ed-8696b66847ae', 'f78893fd-8e2e-4741-9817-4906590e7081', '0', '100', '100', '2', 'CZ20230512151204', '2023-05-12 15:12:04', '2023-05-12 15:12:04');
INSERT INTO `user_balance_record` VALUES ('31835675-c143-4693-a807-f4344efd1483', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '0', '5', '5', '2', 'CZ20230513015001', '2023-05-13 01:50:01', '2023-05-13 01:50:01');
INSERT INTO `user_balance_record` VALUES ('39703679-8add-4cf4-9080-560a5b18b194', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '100', '5', '95', '1', 'WL20230513015414', '2023-05-13 01:54:14', '2023-05-13 01:54:14');
INSERT INTO `user_balance_record` VALUES ('3e13b9f8-f39a-4147-b65e-f3e589202f5e', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '0', '100', '100', '2', 'CZ20230513015307', '2023-05-13 01:53:07', '2023-05-13 01:53:07');
INSERT INTO `user_balance_record` VALUES ('4d47785c-3bbe-4a69-ac06-5bb0db426f28', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '200', '5', '195', '1', 'WL20230512143747', '2023-05-12 14:37:47', '2023-05-12 14:37:47');
INSERT INTO `user_balance_record` VALUES ('4f92c474-a327-4225-9c3e-f43d59322660', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '245', '5', '250', '2', 'WL20230513012800', '2023-05-13 01:28:35', '2023-05-13 01:28:35');
INSERT INTO `user_balance_record` VALUES ('570b227e-48a9-4558-b0bd-3d7c7cd2826d', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '5', '5', '0', '1', 'WL20230513015034', '2023-05-13 01:50:34', '2023-05-13 01:50:34');
INSERT INTO `user_balance_record` VALUES ('61050f49-7883-49f0-8d88-c6340d0c0988', '4eb975c6-2066-4f20-804a-6f4c8c6c2d38', '0', '100', '100', '2', 'CZ20230513112838', '2023-05-13 11:28:38', '2023-05-13 11:28:38');
INSERT INTO `user_balance_record` VALUES ('782af757-cbda-4d88-a667-047f1ba4ed29', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '285', '5', '290', '2', 'WL20230512143747', '2023-05-13 01:13:46', '2023-05-13 01:13:46');
INSERT INTO `user_balance_record` VALUES ('8eb7c006-1f7b-4ce5-aa2c-7c44ea52b319', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '70', '50', '120', '2', 'CZ20230513144833', '2023-05-13 14:48:33', '2023-05-13 14:48:33');
INSERT INTO `user_balance_record` VALUES ('8fd85b51-2667-435f-b28d-5ad4e91e1b74', 'd8f59a25-3577-489a-9ef7-1cb4cad5887b', '20', '5', '15', '1', 'WL20230513125439', '2023-05-13 12:54:39', '2023-05-13 12:54:39');
INSERT INTO `user_balance_record` VALUES ('97d710bc-36bc-4db2-afa7-ffbb6508c924', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '250', '5', '255', '0', 'WL20230513015414', '2023-05-13 02:27:18', '2023-05-13 02:27:18');
INSERT INTO `user_balance_record` VALUES ('9d674aa5-6be5-4432-ab4e-e39f974138a5', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '195', '5', '190', '1', 'WL20230512143900', '2023-05-12 14:39:00', '2023-05-12 14:39:00');
INSERT INTO `user_balance_record` VALUES ('b71b14b6-4c6e-4f0b-9f81-7d5108226f89', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '260', '5', '255', '1', 'WL20230513012130', '2023-05-13 01:21:30', '2023-05-13 01:21:30');
INSERT INTO `user_balance_record` VALUES ('bdb14570-e308-47c7-9336-c9b70284a006', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '100', '30', '70', '1', 'WL20230513144555', '2023-05-13 14:45:55', '2023-05-13 14:45:55');
INSERT INTO `user_balance_record` VALUES ('c64c13df-19b8-4eee-a38b-eae307e36601', 'd8f59a25-3577-489a-9ef7-1cb4cad5887b', '0', '20', '20', '2', 'CZ20230513125358', '2023-05-13 12:53:58', '2023-05-13 12:53:58');
INSERT INTO `user_balance_record` VALUES ('ca663c93-f15b-43a1-974a-7e943389cabd', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '190', '5', '185', '1', 'WL20230512144003', '2023-05-12 14:40:03', '2023-05-12 14:40:03');
INSERT INTO `user_balance_record` VALUES ('d01f3dec-6c21-4a51-833c-b68323aee34b', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '120', '100', '220', '2', 'CZ20230513144902', '2023-05-13 14:49:02', '2023-05-13 14:49:02');
INSERT INTO `user_balance_record` VALUES ('d0c514e4-7833-4ffe-8355-2aaaf8d5f0ca', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '185', '5', '180', '1', 'WL20230512144038', '2023-05-12 14:40:38', '2023-05-12 14:40:38');
INSERT INTO `user_balance_record` VALUES ('d3638acb-5514-4823-a131-43ba306e6598', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '290', '5', '285', '1', 'WL20230513011535', '2023-05-13 01:15:35', '2023-05-13 01:15:35');
INSERT INTO `user_balance_record` VALUES ('daaeb3bf-5edd-41e8-afbd-d31a46645ca4', '01ed882b-efdc-4706-8c60-844d1b7b0e56', '95', '5', '100', '0', 'WL20230513011846', '2023-05-13 12:30:06', '2023-05-13 12:30:06');
INSERT INTO `user_balance_record` VALUES ('e751a493-9cb7-4d26-9f52-fcc25440664c', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '280', '5', '285', '2', 'WL20230512144003', '2023-05-13 01:13:43', '2023-05-13 01:13:43');
INSERT INTO `user_balance_record` VALUES ('edb26d4a-2dac-40f9-9e16-76eb6515c507', '1883bd9b-b896-4d9c-86f6-f2b4222f033b', '270', '10', '260', '1', 'WL20230513012002', '2023-05-13 01:20:02', '2023-05-13 01:20:02');
INSERT INTO `user_balance_record` VALUES ('f1e0bfc3-8814-4bea-a3e9-34b70fca6d8f', 'f78893fd-8e2e-4741-9817-4906590e7081', '200', '5', '205', '0', 'WL20230512143900', '2023-05-12 16:49:04', '2023-05-12 16:49:04');
INSERT INTO `user_balance_record` VALUES ('f5bb200d-710e-4dac-beed-66cbaf99f80a', 'f78893fd-8e2e-4741-9817-4906590e7081', '100', '100', '200', '2', 'CZ20230512151316', '2023-05-12 15:13:16', '2023-05-12 15:13:16');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `uid` varchar(45) NOT NULL COMMENT '用户id',
  `userPhoto` char(255) DEFAULT NULL COMMENT '头像地址',
  `userName` varchar(45) DEFAULT NULL COMMENT '用户姓名',
  `userGender` tinyint(1) DEFAULT '1' COMMENT '用户性别：1:男，0:女',
  `userAddress` varchar(255) DEFAULT NULL,
  `userPhone` varchar(45) NOT NULL COMMENT '用户电话',
  `userCollege` varchar(50) DEFAULT '' COMMENT '所在学院',
  `userEnterYear` int(11) DEFAULT '0' COMMENT '入学年份',
  `userRole` tinyint(11) DEFAULT '0' COMMENT '用户角色：0 普通用户 1 配送员 2 管理员',
  `userStudentCard` varchar(255) DEFAULT NULL COMMENT '学生证',
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('01ed882b-efdc-4706-8c60-844d1b7b0e56', 'http://192.168.191.1:3001/api/public/getFiles?id=9d362958843f135c1eec477e9d997287.jpg&&mimetype=image/jpeg', '赵盼儿', '0', '逸夫楼', '15754317792', '艺术学院', '2020', '1', 'http://192.168.191.1:3001/api/public/getFiles?id=3ee54312e242a3ab46a83513585bbea8.jpg&&mimetype=image/jpeg', '2023-05-13 01:48:10', '2023-05-13 01:48:20');
INSERT INTO `user_info` VALUES ('1883bd9b-b896-4d9c-86f6-f2b4222f033b', 'http://localhost:3001/api/public/getFiles?id=77998c9a7e34e18560ab95532f00e64f.jpg&&mimetype=image/jpeg', '小王', '0', '13舍', '19330668663', '计算机与电气工程学院', '2023', '1', 'http://localhost:3001/api/public/getFiles?id=1c568f12d445dececb7c499bc7b13af8.jpg&&mimetype=image/jpeg', '2023-05-12 14:27:41', '2023-05-13 01:51:38');
INSERT INTO `user_info` VALUES ('4eb975c6-2066-4f20-804a-6f4c8c6c2d38', 'http://192.168.0.103:3001/api/public/getFiles?id=274489af9ef00f210c7ade381e6c083e.jpg&&mimetype=image/jpeg', '陈赫', '1', '文理男生宿舍581', '13538420906', '生物学院', '2021', '1', 'http://192.168.0.103:3001/api/public/getFiles?id=452d293d54e3a95f7273ab886ac574d5.jpg&&mimetype=image/jpeg', '2023-05-13 11:24:38', '2023-05-13 14:51:56');
INSERT INTO `user_info` VALUES ('d8f59a25-3577-489a-9ef7-1cb4cad5887b', 'http://192.168.0.103:3001/api/public/getFiles?id=82af9663d94f8ec5b1f667ad7bb356a2.jpg&&mimetype=image/jpeg', '王二狗', '1', '男生宿舍501', '17834521386', '化学学院', '2021', '1', 'http://192.168.0.103:3001/api/public/getFiles?id=14d62ce9b81be79733bf43b5c4afd2f1.jpg&&mimetype=image/jpeg', '2023-05-13 12:43:20', '2023-05-13 12:55:45');
INSERT INTO `user_info` VALUES ('f78893fd-8e2e-4741-9817-4906590e7081', 'http://localhost:3001/api/public/getFiles?id=cb9f8b60f18d3cefdb6a30b75a29b200.jpg&&mimetype=image/jpeg', '胡美庆', '0', '第三教学楼', '15211343695', '计算机与电气工程学院', '2023', '1', 'http://localhost:3001/api/public/getFiles?id=3f694f164605995c74d0f77b3e2649ad.jpg&&mimetype=image/jpeg', '2023-05-12 14:57:48', '2023-05-12 17:59:49');
