const db = require("../models");
const logger = require("../utils/utils.logger").logger();
const DAO = require("../dao/DAO");
const Users = db.users;
const UserInfo = db.userInfo;
const UserBalance = db.userBalance;

// 注册用户账号
exports.registerUser = (req, res) => {
  const { userphone, password } = req.body;
  // 请求验证
  if (!userphone)
    return res.sendResult({ data: "", code: 500, message: "手机号不能为空！" });

  if (!password)
    return res.sendResult({ data: "", code: 500, message: "密码不能为空！" });

  const user = {
    userphone,
    password,
    userStatus: "0",
  };
  Users.findOne({ where: { userphone: userphone } }).then((singleUser) => {
    if (singleUser && singleUser.uid)
      return res.sendResultAto(null, 605, "用户账号已存在！");
    DAO.create(Users, user, async (result) => {
      logger.debug(
        `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
          user
        )}; 响应：${JSON.stringify(result)}`
      );
      const userBalanceInfo = {
        uid: result.data.uid,
        userBalance: 0,
      };
      await UserBalance.create(userBalanceInfo);

      res.sendResult(result);
    });
  });
};

// 创建用户信息
exports.createUserInfo = (req, res) => {
  const {
    uid,
    userName,
    userGender,
    userPhone,
    userCollege,
    userEnterYear,
    userStudentCard,
    userAddress,
    userPhoto
  } = req.body;
  // 请求验证
  if (!uid)
    return res.sendResult({ data: "", code: 500, message: "用户ID不能为空！" });

  if (!userName)
    return res.sendResult({
      data: "",
      code: 500,
      message: "用户姓名不能为空！",
    });
  if (!userGender)
    return res.sendResult({
      data: "",
      code: 500,
      message: "用户性别不能为空！",
    });

  if (!userPhone)
    return res.sendResult({
      data: "",
      code: 500,
      message: "用户手机号不能为空！",
    });

  const userInfo = {
    uid,
    userName,
    userGender,
    userPhone,
    userCollege,
    userEnterYear,
    userRole: "1",
    userStudentCard,
    userAddress,
    userPhoto
  };
  UserInfo.findOne({ where: { uid } }).then((singleFriends) => {
    if (singleFriends && singleFriends.uid)
      return res.sendResultAto(null, 505, "用户信息已存在！");
    DAO.create(UserInfo, userInfo, (data) => {
      res.sendResult(data);
    });
  });
};

// Retrieve all user from the database.
exports.findAll = (req, res) => {
  const pm = req.body;
  DAO.list(Users, pm, (list) => {
    logger.debug(
      `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
        pm
      )}; 响应：${JSON.stringify(list)}`
    );
    res.sendResult(list);
  });
};

// Find a single Users with an id
exports.findOne = (req, res) => {
  const pm = req.body;
  DAO.findOne(Users, pm, (data) => {
    logger.debug(
      `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
        pm
      )}; 响应：${JSON.stringify(data)}`
    );
    res.sendResult(data);
  });
};

// Update a Users by the id in the request
exports.updateUserInfo = (req, res) => {
  const pm = req.body;
  // 请求验证
  if (!pm.uid)
    return res.sendResult({ data: "", code: 500, message: "ID不能为空！" });
  DAO.update(UserInfo, pm, { uid: pm.uid }, (data) => {
    logger.debug(
      `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
        pm
      )}; 响应：${JSON.stringify(data)}`
    );
    res.sendResult(data);
  });
};

// Delete a Users with the specified id in the request
exports.delete = (req, res) => {
  const pm = req.body;
  // 请求验证
  if (!pm.id)
    return res.sendResult({ data: "", code: 500, message: "ID不能为空！" });
  DAO.delete(Users, { id: pm.id }, (data) => {
    logger.debug(
      `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
        pm
      )}; 响应：${JSON.stringify(data)}`
    );
    res.sendResult(data);
  });
};

// Delete all Users from the database.
exports.deleteAll = (req, res) => {
  const pm = req.body;
  DAO.deleteAll(Users, (data) => {
    logger.debug(
      `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
        pm
      )}; 响应：${JSON.stringify(data)}`
    );
    res.sendResult(data);
  });
};

// Delete Users from the database.
exports.query = (req, res) => {
  const pm = req.body;
  let sql = "SELECT * FROM `users`";
  DAO.doQuery(sql, (data) => {
    logger.debug(
      `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
        pm
      )}; 响应：${JSON.stringify(data)}`
    );
    res.sendResult(data);
  });
};
