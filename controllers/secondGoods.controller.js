const moment = require("moment");
const db = require("../models");
const logger = require("../utils/utils.logger").logger();
const DAO = require("../dao/DAO");
// SecondGoodsMarket 二手市场表 secondGoods
const SecondGoodsMarket = db.secondGoods;

// 创建一个新的二手商品出售单
exports.create = async (req, res) => {
  const pm = req.body;
  // 请求验证
  if(!pm.goodsName)
    return res.sendResult({
      data:"",
      code:605,
      message:"商品名称不能为空！",
    })
  // Create a SecondGoods
  const goods = {
    uid: pm.uid,
    goodsId: `ES${moment().format("YYYYMMDDHHmmss")}`,
    goodsName: pm.goodsName,
    oldPrice:pm.oldPrice,
    newPrice:pm.newPrice,
    phone: pm.phone,
    inventory:pm.inventory,
    degree:pm.degree,
    goodsDescribe:pm.goodsDescribe,
    goodsPhoto:pm.goodsPhoto,
    goodsStatus:'0'
  };

  DAO.create(SecondGoodsMarket, goods, async (result) => {
    logger.debug(
      `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
        pm
      )}; 响应：${JSON.stringify(result)}`
    );
    
    res.sendResult(result);
  });
};

// 查询所有的二手商品信息
exports.findAll = (req, res) => {
   const pm = {
    sort: {
      prop: "createdAt",
      order: "desc",
    },
  };
  DAO.list(SecondGoodsMarket, pm, (list) => {
    logger.debug(
      `${req.method} ${req.baseUrl + req.path} *** 参数：${JSON.stringify(
        pm
      )}; 响应：${JSON.stringify(list)}`
    );
    res.sendResult(list);
  });
};

// 删除二手商品信息
exports.deleteGoods = (req, res) => {
  const pm = req.query;
  // 请求验证
  if (!pm.goodsId)
    return res.sendResult({ data: "", code: 605, message: "失物招领ID不能为空！" });
   
    DAO.delete(
      SecondGoodsMarket,
      { goodsId: pm.goodsId },
      async (updateResult) => {
        res.sendResult(updateResult);
      }
    );
};
