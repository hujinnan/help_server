const db = require("../models");
const DAO = require("../dao/DAO");

const UserBalanceRecord = db.userBalanceRecord;

// 创建用户余额变更信息
exports.createUserBalanceChangeInfo = (req, res) => {
  const { userId, oldBalance, amount, newBalance, type, orderId } = req.body;
  // 请求验证
  if (!userId)
    return res.sendResult({ data: "", code: 500, message: "用户ID不能为空！" });

  if (type === undefined || type === null)
    return res.sendResult({
      data: "",
      code: 500,
      message: "交易类型不能为空！",
    });

  const userBalanceInfo = {
    uid: userId,
    oldBalance,
    amount,
    newBalance,
    type,
    orderId,
  };
  DAO.create(UserBalanceRecord, userBalanceInfo, (data) => {
    res.sendResult(data);
  });
};
