/**
 * @typedef User
 * @property {string} userphone.required  - 用户账号(手机号)
 * @property {string} password.required  - 用户密码
 */

/**
 * @typedef UserInfo
 * @property {string} uid.required  - 用户id
 * @property {string} userPhoto  - 头像地址
 * @property {string} userName.required  - 用户姓名
 * @property {integer} userGender.required  - 用户性别：0:女,1:男
 * @property {string} userPhone.required  - 用户电话
 * @property {string} userCollege  - 所在学院
 * @property {string} userEnterYear  - 入学年份
 * @property {string} userStudentCard  - 学生证
 */

/**
 * @typedef Order
 * @property {string} orderid.required  - 订单号
 * @property {string} uid  - 用户id
 * @property {string} userName  - 用户姓名
 * @property {string} did   - 配送员id
 * @property {integer} orderType  - 订单类型：0：帮我取，1:帮我寄，2:帮我买，3:万能帮
 * @property {string} sourceAddress  - 源地址
 * @property {string} sourcePhone  - 源电话
 * @property {string} targetAddress  - 目标地址
 * @property {string} targetPhone  - 目标电话
 * @property {string} remark  - 购买要求／备注信息
 * @property {string} requireTime  - 送货时间
 * @property {string} timeLong  - 尽快送达／所需时常（秒或毫秒）
 * @property {string} amount  - 跑腿费
 * @property {string} recivedAt  - 接单时间
 * @property {string} finishedAt  - 订单完成时间
 * @property {string} canceledAt  - 订单取消时间
 * @property {string} reciveCode  - 收货码
 * @property {string} itemSize  - 物品重量/大小
 * @property {string} itemCount  - 快递数量
 * @property {string} sexLimit  - 性别限制：0:不限性别 1:限男生 2:限女生
 */

/**
 * @typedef FinishOrder
 * @property {string} orderid.required  - 订单号
 * @property {string} userId   - 配送员id
 * @property {string} reciveCode  - 收货码
 */

/**
 * @typedef UserBalance
 * @property {string} userId   - 用户id
 * @property {number} userBalance  - 用户余额
 */

/**
 * @typedef UserAddress
 * @property {string} userId   - 用户id
 * @property {number} userAddress  - 用户地址
 */

/**
 * @typedef UpdateUserAddress
 * @property {string} id   - 用户地址id
 * @property {number} userAddress  - 用户地址
 */

/**
 * @typedef lostAndFind
 * @property {string} lostOrFindOrderId.required      - 失物招领id
 * @property {string} uid   -用户id
 * @property {string} lostOrFindAddress   -丢失物品源地址
 * @property {string} phone     -联系电话
 * @property {string} targetAddress     -联系地址
 * @property {string} remark  -备注
 * @property {string} describe  -物品描述
 * @property {string} lostOrFindTime    -丢失时间或拾到时间
 * @property {string} lostOrderType  -失物招领类型，0代表失物，1代表寻物
 * @property {string} propertyPicture -物品图片
*/


/**
 * @typedef secondGoods
 * @property {string} goodsId.required      - 二手商品id
 * @property {string} uid   -用户id
 * @property {string} goodsName   -二手商品名称
 * @property {number} oldPrice     -原价
 * @property {number} newPrice  -现价
 * @property {string} phone  -联系电话
 * @property {string} inventory    -库存
 * @property {string} degree     -新旧程度
 * @property {string} goodsDescribe  -商品描述
 * @property {string} goodsPhoto    -商品图片
 * @property {string} goodsStatus    -商品状态，0未出售，1已出售
 */