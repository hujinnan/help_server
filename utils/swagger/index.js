const options = {
  swaggerDefinition: {
    info: {
      title: "文理帮APP",
      version: "1.0.0",
      description: `文理帮APP接口api`,
    },
    host: `${process.env.SWEG_URL}:${process.env.DEV_PORT}`,
    basePath: "/",
    produces: ["application/json", "application/xml"],
    schemes: ["http", "https"],
    securityDefinitions: {
      JWT: {
        type: "apiKey",
        in: "header",
        name: "Authorization",
        description: "",
      },
    },
  },
  route: {
    url: "/swagger", //打开swagger文档页面地址
    docs: "/swagger.json", //swagger文件 api
  },
  basedir: __dirname, //app absolute path

  files: [
    //在那个文件夹下面收集注释
    "../../routes/api/private/*.js",
    "../../routes/api/public/*.js",
    "../../utils/swagger/*.js",
  ],
};

module.exports = options;
