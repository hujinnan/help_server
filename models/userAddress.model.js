const db = require("./index");

module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "userAddress",
    {
      id: {
        type: Sequelize.UUID,
        notNull: true,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4, // 或 DataTypes.UUIDV1
        comment: "地址id",
      },
      uid: {
        type: Sequelize.UUID,
        notNull: true,
        notEmpty: true,
        comment: "用户id",
      },
      userAddress: {
        type: Sequelize.STRING,
        comment: "地址",
      },
    },
    {
      tableName: "user_address",
    }
  );
};
