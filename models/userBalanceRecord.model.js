const db = require("./index");

module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "userBalanceRecord",
    {
      id: {
        type: Sequelize.UUID,
        notNull: true,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4, // 或 DataTypes.UUIDV1
        comment: "用户交易记录id",
      },
      uid: {
        type: Sequelize.UUID,
        notNull: true,
        notEmpty: true,
        comment: "用户id",
      },
      oldBalance: {
        type: Sequelize.DOUBLE,
        comment: "老余额",
      },
      amount: {
        type: Sequelize.DOUBLE,
        comment: "修改金额",
      },
      newBalance: {
        type: Sequelize.DOUBLE,
        comment: "新余额",
      },
      type: {
        type: Sequelize.INTEGER,
        comment: "交易类型 0:接单入款，1:发单出款，2:取消订单，3:充值",
      },
      orderId: {
        type: Sequelize.STRING,
        comment: "交易订单流水号",
      },
    },
    {
      tableName: "user_balance_record",
    }
  );
};
