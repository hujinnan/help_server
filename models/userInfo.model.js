module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "userInfo",
    {
      uid: {
        type: Sequelize.UUID,
        notNull: true,
        primaryKey: true,
        comment: "用户id",
      },
      userPhoto: {
        type: Sequelize.STRING,
        comment: "头像地址",
      },
      userName: {
        type: Sequelize.STRING,
        comment: "用户姓名",
      },
      userGender: {
        type: Sequelize.INTEGER,
        comment: "用户性别：0:女,1:男",
      },
      userPhone: {
        type: Sequelize.STRING,
        comment: "用户电话",
      },
      userCollege: {
        type: Sequelize.STRING,
        comment: "所在学院",
      },
      userEnterYear: {
        type: Sequelize.STRING,
        comment: "入学年份",
      },
      userRole: {
        type: Sequelize.STRING,
        comment: "用户角色：0 普通用户 1 配送员 2 管理员",
      },
      userStudentCard: {
        type: Sequelize.STRING,
        comment: "学生证",
      },
      userAddress: {
        type: Sequelize.STRING,
        comment: "取货地址",
      },
    },
    {
      tableName: "user_info",
    }
  );
};
