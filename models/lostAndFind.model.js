const db = require("./index");

module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "lostPropertyOrder",
    {
      lostOrFindOrderId: {
        type: Sequelize.STRING,
        notNull: true,
        primaryKey: true,
        comment: "失物单号",
      },
      uid: {
        type: Sequelize.UUID,
        notNull: true,
        notEmpty: true,
        comment: "用户id",
      },
      lostOrFindAddress: {
        type: Sequelize.STRING,
        comment: "丢失地址或拾到地址",
      },
      phone: {
        type: Sequelize.STRING,
        comment: "联系电话",
      },
      targetAddress: {
        type: Sequelize.STRING,
        comment: "联系地址",
      },
      remark: {
        type: Sequelize.STRING,
        notNull: true,
        comment: "备注",
      },
      describe: {
        type: Sequelize.STRING,
        notNull: true,
        comment: "物品描述",
      },
      lostOrFindTime: {
        type: Sequelize.DATE,
        comment: "丢失时间或拾到时间",
      },
      lostOrderType: {
        type: Sequelize.STRING,
        comment: "失物招领类型，0代表失物，1代表寻物",
      },
      propertyPicture:{
        type: Sequelize.STRING,
        notNull: true,
        comment: "物品图片",
      }
    },
    {
      tableName: "lost_property_order",
    }
  );
};
