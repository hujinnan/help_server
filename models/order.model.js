const db = require("./index");

module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "order",
    {
      orderid: {
        type: Sequelize.STRING,
        notNull: true,
        primaryKey: true,
        comment: "订单号",
      },
      uid: {
        type: Sequelize.UUID,
        notNull: true,
        notEmpty: true,
        comment: "用户id",
      },
      userName: {
        type: Sequelize.STRING,
        notNull: true,
        notEmpty: true,
        comment: "用户姓名",
      },
      did: {
        type: Sequelize.UUID,
        comment: "配送员id",
      },
      orderType: {
        type: Sequelize.STRING,
        notNull: true,
        comment: "订单类型：0：帮我取，1:帮我寄，2:帮我买，3:万能帮",
      },
      sourceAddress: {
        type: Sequelize.STRING,
        comment: "源地址",
      },
      sourcePhone: {
        type: Sequelize.STRING,
        comment: "源电话",
      },
      targetAddress: {
        type: Sequelize.STRING,
        comment: "目标地址",
      },
      targetPhone: {
        type: Sequelize.STRING,
        comment: "目标电话",
      },
      remark: {
        type: Sequelize.STRING,
        notNull: true,
        comment: "购买要求／备注信息",
      },
      requireTime: {
        type: Sequelize.DATE,
        comment: "送货时间／发货时间／取货时间／排队时间",
      },
      timeLong: {
        type: Sequelize.STRING,
        comment: "尽快送达／所需时常（秒或毫秒）",
      },
      amount: {
        type: Sequelize.DOUBLE,
        notNull: true,
        comment: "跑腿费",
      },
      recivedAt: {
        type: Sequelize.DATE,
        comment: "接单时间",
      },
      finishedAt: {
        type: Sequelize.DATE,
        comment: "订单完成时间",
      },
      canceledAt: {
        type: Sequelize.DATE,
        comment: "订单取消时间",
      },
      reciveCode: {
        type: Sequelize.STRING,
        comment: "收货码",
      },
      status: {
        type: Sequelize.STRING,
        comment: "订单状态：0:待接单，1:已接单，2:已完成，3:已取消",
      },
      itemSize: {
        type: Sequelize.STRING,
        comment: "物品重量/大小",
      },
      itemCount: {
        type: Sequelize.INTEGER,
        comment: "快递数量",
      },
      sexLimit: {
        type: Sequelize.INTEGER,
        comment: "性别限制：0:不限性别 1:限男生 2:限女生",
      },
    },
    {
      tableName: "order",
    }
  );
};
