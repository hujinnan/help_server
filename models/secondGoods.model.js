const db = require("./index");

module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "secondGoods",
    {
      goodsId: {
        type: Sequelize.UUID,
        notNull: true,
        primaryKey: true,
        comment: "二手商品Id",
      },
      uid: {
        type: Sequelize.STRING,
        notNull: true,
        notEmpty: true,
        comment: "用户id",
      },
      goodsName: {
        type: Sequelize.STRING,
        notNull: true,
        notEmpty: true,
        comment: "商品名称",
      },
      oldPrice: {
        type: Sequelize.DOUBLE,
        comment: "商品原价",
      },
      newPrice: {
        type: Sequelize.DOUBLE,
        notNull: true,
        notEmpty: true,
        comment: "商品现价",
      },
      phone: {
        type: Sequelize.STRING,
        notNull: true,
        comment: "联系电话",
      },
      inventory: {
        type: Sequelize.DOUBLE,
        comment: "库存",
      },
      degree: {
        type: Sequelize.STRING,
        comment: "新旧程度",
      },
      goodsDescribe: {
        type: Sequelize.STRING,
        comment: "商品描述",
      },
      goodsPhoto: {
        type: Sequelize.STRING,
        comment: "商品图片",
      },
      goodsStatus: {
        type: Sequelize.STRING,
        comment: "商品状态，0未出售，1已出售",
      },
    },
    {
      tableName: "second_goods",
    }
  );
};
