const db = require("./index");

module.exports = (sequelize, Sequelize) => {
  return sequelize.define(
    "userBalance",
    {
      uid: {
        type: Sequelize.UUID,
        notNull: true,
        notEmpty: true,
        primaryKey: true,
        comment: "用户id",
      },
      userBalance: {
        type: Sequelize.DOUBLE,
        comment: "用户余额",
      },
    },
    {
      tableName: "user_balance",
    }
  );
};
