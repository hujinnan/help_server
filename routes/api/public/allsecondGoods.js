const express = require("express");
const router = express.Router();
const secondGoods = require("../../../controllers/secondGoods.controller");

/**
 * 查询所有二手商品列表
 * @route GET /api/public/allsecondGoods/findAll
 * @group 跳蚤市场管理
 * @param {object} query - 请按固定查询规范
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 */
router.get("/findAll", secondGoods.findAll);

//Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VycGhvbmUiOiIxNTc1NDMxNzc5MiIsInBhc3N3b3JkIjoiMTIzNDU2IiwiaWF0IjoxNjgyOTA2MzEyLCJleHAiOjE2ODMxNjU1MTJ9.P3MqGUyQK0cXPjKlRVhOkf-z734l0tnBsNvPgLHZwF8
module.exports = router;
