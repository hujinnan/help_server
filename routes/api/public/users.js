let express = require("express");
let router = express.Router();
const Users = require("../../../controllers/users.controller");

/**
 * 注册新用户
 * @route POST /api/public/users/register
 * @group 用户注册
 * @param {User.model} Users.body.required - 账号信息
 * @returns {object} 200
 * @returns {Error}  default - Unexpected error
 */
router.post("/register", Users.registerUser);

/**
 * 创建新用户信息
 * @route POST /api/public/users/createUserInfo
 * @group 用户注册
 * @param {UserInfo.model} UserInfo.body.required - 用户信息
 * @returns {object} 200
 * @returns {Error}  default - Unexpected error
 */
router.post("/createUserInfo", Users.createUserInfo);

module.exports = router;
