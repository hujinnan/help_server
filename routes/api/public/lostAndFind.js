const express = require("express");
const router = express.Router();
const LostAndFind = require("../../../controllers/lostAndFind.controller");

/**
 * 查询失物招领列表
 * @route GET /api/public/lostAndFind/list
 * @group 失物招领管理
 * @param {object} query - 请按固定查询规范
 * @returns {object} 200 - An array of friends info
 * @returns {object} 605 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/list", LostAndFind.findAll);


module.exports = router;
