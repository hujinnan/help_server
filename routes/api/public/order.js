const express = require("express");
const router = express.Router();
const Order = require("../../../controllers/order.controller");

/**
 * 查询top4待帮忙帮单列表
 * @route GET /api/private/order/top4List
 * @group 帮单管理
 * @param {object} query - 请按固定查询规范
 * @returns {object} 200 - An array of friends info
 * @returns {object} 605 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/top4List", Order.findTop4List);


/**
 * 查询帮单列表
 * @route GET /api/private/order/list
 * @group 帮单管理
 * @param {object} query - 请按固定查询规范
 * @returns {object} 200 - An array of friends info
 * @returns {object} 605 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/list", Order.findAll);

module.exports = router;