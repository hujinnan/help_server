const express = require("express");
const router = express.Router();
const secondGoods = require("../../../controllers/secondGoods.controller");

/**
 * 创建二手商品
 * @route POST /api/private/secondGoods/create
 * @group 跳蚤市场管理
 * @param {secondGoods.model} secondGoods.body.required - 二手商品信息
 * @returns {object} 200 -
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.post("/create", secondGoods.create);


/**
 * 删除二手商品
 * @route GET /api/private/secondGoods/deleteGoods
 * @group 跳蚤市场管理
 * @param  {string} goodsId.query.required - 请输入二手商品ID
 * @returns {object} 200 -
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/deleteGoods", secondGoods.deleteGoods);

//Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VycGhvbmUiOiIxNTc1NDMxNzc5MiIsInBhc3N3b3JkIjoiMTIzNDU2IiwiaWF0IjoxNjgyOTA2MzEyLCJleHAiOjE2ODMxNjU1MTJ9.P3MqGUyQK0cXPjKlRVhOkf-z734l0tnBsNvPgLHZwF8
module.exports = router;
