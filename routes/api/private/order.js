const express = require("express");
const router = express.Router();
const Order = require("../../../controllers/order.controller");

/**
 * 创建帮单
 * @route POST /api/private/order/create
 * @group 帮单管理
 * @param {Order.model} Order.body.required - 帮单信息
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.post("/create", Order.create);



/**
 * 接帮单
 * @route GET /api/private/order/reciveOrder
 * @group 帮单管理
 * @param {string} orderid.query.required - 帮单ID
 * @param {string} userId.query.required - 用户ID
 * @param {string} userGender.query.required - 用户性别
 * @returns {object} 200 - An array of friends info
 * @returns {object} 605 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/reciveOrder", Order.reciveOrder);

/**
 * 完成帮单
 * @route POST /api/private/order/finishOrder
 * @group 帮单管理
 * @param {FinishOrder.model} FinishOrder.body.required - 帮单信息
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.post("/finishOrder", Order.finishOrder);

/**
 * 取消帮单
 * @route GET /api/private/order/cancelOrder
 * @group 帮单管理
 * @param {string} orderid.query.required - 帮单ID
 * @param {string} userId.query.required - 用户ID
 * @returns {object} 200 -  
 * @returns {object} 605 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/cancelOrder", Order.cancelOrder);

module.exports = router;
