const express = require("express");
const router = express.Router();
const UserBalance = require("../../../controllers/userBalance.controller");

/**
 * 创建用户余额
 * @route POST /api/private/userBalance/create
 * @group 用户余额管理
 * @param {UserBalance.model} UserBalance.body.required - 用户余额信息
 * @returns {object} 200 -
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.post("/create", UserBalance.createUserBalanceInfo);

/**
 * 更新用户余额
 * @route POST /api/private/userBalance/updateUserBalanceInfo
 * @group 用户余额管理
 * @param {UserBalance.model} UserBalance.body.required - 用户余额信息
 * @returns {object} 200 -
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.post("/updateUserBalanceInfo", UserBalance.updateUserBalanceInfo);


/**
 * 查询用户余额
 * @route get /api/private/userBalance/getUserBalanceInfo
 * @group 用户余额管理
 * @param {string} userId.query.required - 用户余额信息
 * @returns {object} 200 -
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/getUserBalanceInfo", UserBalance.getUserBalanceInfo);


module.exports = router;
