const express = require("express");
const router = express.Router();
const LostAndFind = require("../../../controllers/lostAndFind.controller");

/**
 * 创建失物招领信息   
 * @route POST /api/private/lostAndFind/create
 * @group 失物招领管理
 * @param {lostAndFind.model} lostAndFind.body.required - 失物招领信息
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.post("/create", LostAndFind.create);


/**
 * 取消失物招领信息
 * @route GET /api/private/lostAndFind/cancelLostOrder
 * @group 失物招领管理
 * @param {string} lostOrFindOrderId.query.required - 失物招领ID
 * @returns {object} 200 -  
 * @returns {object} 605 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/cancelLostOrder", LostAndFind.cancelLostOrder);

module.exports = router;
