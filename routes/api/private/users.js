const express = require("express");
const router = express.Router();
const Users = require("../../../controllers/users.controller");

/**
 * 删除用户信息
 * @route POST /api/private/users/delete
 * @group 用户管理
 * @param {number} id - 请输入用户ID
 * @param {string} authorization - 请输入token
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 */
router.post("/delete", Users.delete);
/**
 * 查询用户信息列表
 * @route POST /api/private/users/list
 * @group 用户管理
 * @param {object} query - 请按固定查询规范
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 */
router.post("/list", Users.findAll);
/**
 * 更新用户信息列表
 * @route POST /api/private/users/updateUserInfo
 * @group 用户管理
 * @param {UserInfo.model} UserInfo.body.required - 用户信息
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 */
router.post("/updateUserInfo", Users.updateUserInfo);

/**
 * 删除全部用户信息
 * @route POST /api/private/users/deleteAll
 * @group 用户管理
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 */
router.post("/deleteAll", Users.deleteAll);

module.exports = router;
