const express = require("express");
const router = express.Router();
const UserAddress = require("../../../controllers/userAddress.controller");

/**
 * 创建用户地址
 * @route POST /api/private/userAddress/create
 * @group 用户地址管理
 * @param {UserAddress.model} UserAddress.body.required - 用户地址信息
 * @returns {object} 200 -
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.post("/create", UserAddress.createUserAddress);

/**
 * 修改用户地址
 * @route POST /api/private/userAddress/updateUserAddress
 * @group 用户地址管理
 * @param {UpdateUserAddress.model} UpdateUserAddress.body.required - 用户地址信息
 * @returns {object} 200 -
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.post("/updateUserAddress", UserAddress.updateUserAddress);


/**
 * 删除用户地址
 * @route DELETE /api/private/userAddress/deleteUserAddress
 * @group 用户地址管理
 * @param  {string} id.query.required - 请输入用户地址ID
 * @returns {object} 200 -
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.delete("/deleteUserAddress", UserAddress.deleteUserAddress);

/**
 * 查询用户地址信息列表
 * @route GET /api/private/userAddress/findUserAllAddress
 * @group 用户地址管理
 * @param {string} userId.query.required - 用户ID
 * @returns {object} 200 - An array of user info
 * @returns {object} 500 - 请求失败
 * @returns {Error}  default - Unexpected error
 * @security JWT
 */
router.get("/findUserAllAddress", UserAddress.findUserAllAddress);

//Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VycGhvbmUiOiIxNTc1NDMxNzc5MiIsInBhc3N3b3JkIjoiMTIzNDU2IiwiaWF0IjoxNjgyOTA2MzEyLCJleHAiOjE2ODMxNjU1MTJ9.P3MqGUyQK0cXPjKlRVhOkf-z734l0tnBsNvPgLHZwF8
module.exports = router;
